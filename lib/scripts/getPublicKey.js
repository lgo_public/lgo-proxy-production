'use strict';

const { Signer } = require('../tools/signer');
const { CryptoService } = require('../tools/cryptoService');

/* eslint-disable no-console */

const libraryPath = process.env['LGO_SIGNER_LIBRARY_PATH'];
if (libraryPath === undefined) {
  console.error('LGO_SIGNER_LIBRARY_PATH must be defined');
  process.exit(1);
}

const pin = process.env['LGO_SIGNER_PIN'];
if (pin === undefined) {
  console.error('Pin is missing');
  process.exit(1);
}

const signer = new Signer({ libraryPath, pin, logger: console });

signer.assertHsmInitialized();
signer.assertPublicKeyAvailable();

printKey();

function printKey() {
  const cryptoService = new CryptoService();
  const publicKey = signer.getRSAPublicKey();
  console.log('Public key:');
  console.log(publicKey);
  console.log('');
  console.log('Fingerprint:');
  console.log(cryptoService.createPublicKeyFingerprint(publicKey));
}
