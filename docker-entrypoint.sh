#!/usr/bin/env bash

set -e

export LGO_EXCHANGE_URL="https://exchange-api.exchange.lgo.markets"
export LGO_WS_URL="wss://ws.exchange.lgo.markets"
export LGO_KEYS_URL="https://storage.googleapis.com/lgo-markets-keys"

if [[ -n "${LGO_SIGNER_PIN_FILE}" ]]; then
  export LGO_SIGNER_PIN="$(< "${LGO_SIGNER_PIN_FILE}")"
fi

if [[ -n "${LGO_ACCESS_KEY_FILE}" ]]; then
  export LGO_ACCESS_KEY="$(< "${LGO_ACCESS_KEY_FILE}")"
fi

if [[ "$1" == "start" ]]; then
  node lib/main
fi

if [[ "$1" == "init" ]]; then
  node lib/scripts/initializeHsm
fi

